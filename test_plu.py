from plu_inpainting.dataloader import data_loader
from plu_inpainting.model import create_model
import numpy as np
from util import util


class PluInpaint:
    def __init__(self, opt):
        # get testing options
        self.opt = opt
        # self.opt.gpu_ids = device if type(device) is list else [device]

        # create a model
        self.model = create_model(self.opt)
        self.model.eval()

    def GetTextRemoval(self, input_path, mask_path):

        # create a dataset with opt.img_file
        print('Inpainting '+input_path)
        self.opt.img_file=input_path
        self.opt.mask_file=mask_path

        dataset = data_loader.dataloader(self.opt)
        dataset_size = len(dataset) * self.opt.batchSize
        print('testing images = %d' % dataset_size)

        for i, data in enumerate(dataset):
            self.model.set_input(data)
            out_paths = self.model.test()

        saved_img = util.path2list(out_paths[0])
        im_sum = np.array(saved_img[0]).sum()

        return saved_img, im_sum
