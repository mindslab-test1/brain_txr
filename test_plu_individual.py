import os
import torch
from PIL import Image
import uuid

# initialize model
# pluInpaint = PluInpaint('', 1)

# torch.manual_seed(0)

FLAG_DEBUG = 1

############### test case 1 ###############
# takes no argument
def test_sample():
    print("Run test_sample()")
    # set input
    inp_img_path = "samples/KakaoTalk_20190520_204528858.jpg"
    mask_path = "./figure/mask/maskKakaoTalk_20190520_204528858.jpg"

    inp_img = _path2list(inp_img_path)
    mask = _path2list(mask_path)

    # set reference output
    ref_img_sum = 656635431

    # run model
    my_img, my_img_sum = pluInpaint.GetTextRemoval(inp_img, mask)
    print(_list2path(my_img))

    # make assertion to check that output is equal to reference
    if FLAG_DEBUG:
        print(my_img_sum)
    else:
        assert (my_img_sum == ref_img_sum)


############### test case 2 ###############
def test_low_res():
    print("Run test_low_res()")
    # set input
    inp_img_path = "samples/KakaoTalk_20190520_204837500.jpg"
    mask_path = "./figure/mask/maskKakaoTalk_20190520_204837500.jpg"

    inp_img = _path2list(inp_img_path)
    mask = _path2list(mask_path)

    # set reference output
    ref_img_sum = 635862068

    # run model
    my_img, my_img_sum = pluInpaint.GetTextRemoval(inp_img, mask)
    print(_list2path(my_img))

    # make assertion to check that output is equal to reference
    if FLAG_DEBUG:
        print(my_img_sum)
    else:
        assert (my_img_sum == ref_img_sum)


############### test case 3 ###############
def test_high_res():
    print("Run test_high_res()")
    # set input
    inp_img_path = "samples/KakaoTalk_20190520_204839671.jpg"
    mask_path = "./figure/mask/maskKakaoTalk_20190520_204839671.jpg"

    inp_img = _path2list(inp_img_path)
    mask = _path2list(mask_path)

    # set reference output
    ref_img_sum = 528597003

    # run model
    my_img, my_img_sum = pluInpaint.GetTextRemoval(inp_img, mask)
    print(_list2path(my_img))

    # make assertion to check that output is equal to reference
    if FLAG_DEBUG:
        print(my_img_sum)
    else:
        assert (my_img_sum == ref_img_sum)


def _path2list(img_path):
    im = Image.open(img_path)
    return list(im.getdata()), im.mode, im.size, im.format

def _list2path(one_img):
    im_list, im_mode, im_size, im_format = one_img
    im = Image.new(im_mode, im_size)
    im.putdata(im_list)
    os.makedirs('tmp', exist_ok=True)
    random_path = os.path.join('tmp', str(uuid.uuid4())+'.'+im_format)

    im.save(random_path)

    return random_path

if __name__ == '__main__':
    test_sample()
    test_low_res()
    test_high_res()