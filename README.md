# brain-ip
Object Removal의 기반 Inpainting 엔진

## Engine information
* **Engine name:** brain-ip

## Software / hardware requirements
* **OS:** Ubuntu18.04
* **Python version:** MUST be >= 3.6
* **Pytorch / Tensorflow version:**  PyTorch 1.4.0
* **minimum GPU spec:** V100 / DRAM >= 6GB
* V100 26번 서버 docker image brain-ip 환경

## Model file:
* **Class path:** test_pan.py, test_plu.py
* **Checkpoint / saved model file path:**

1. {project_root}/model_logs/PAN_pth에 다음 파일을 위치시킨다. [download_link](https://drive.google.com/file/d/1UY0K2JPsUmqmaJ68k2Q6KwByhogF1Usv/view?usp=sharing)

1. {project_root}/model_logs/celeba_random에 다음 파일들을 위치시킨다. [download_link](https://drive.google.com/drive/folders/1nLq-W7eAZErqsvB1Q8h1yQT_l7_kZBXT)

## Install
1. `pip install -r requirement.txt`
1. `./compile.sh`

혹은 compile.sh를 실행하는 대신 다음과 같이 c++ file build
1. `cd ./pan/models/post_processing/pa/`
1. `python setup.py build_ext --inplace`
1. `cd ../../../../`

## Docker run
Port: `56001`

Image build: 
* `cd {project_root}`
* `docker build -t brain-ip .`

Container 실행: 
* `docker run --name ip_server --ipc=host --gpus 'device=GPU_IDS' --rm -it brain-ip /bin/bash` 
* (GPU_IDS에 할당 GPU 입력. 예시: 'device=1')

client.py 테스트:
* `docker exec -it ip_server bash`
* `/app/venv_brain-ip/bin/python3 client.py --port 56001 --input {Image File} --filename result.jpg`
* (`{Image File}`: `samples/KakaoTalk_20190520_204528858.jpg`, ...)

## Test code:

- [x] Do you have **at least 3** test cases?

* **Engine throughput:** 평균 4s/1장  (1440x1440 이미지 ) - 50회 가량 inpaint를 시도 후 가장 좋은 output을 선택한다.
(--nsamples) 1회 sample할 때 기준: 평균 0.3s/1장 
* **Test file path:** {project_root}/test.py
* **test input file path:** {project_root}/figure 내에 있는 image들

* **test output file path:** {project_root}/figure/mask, {project_root}/figure/result (변경 가능)
* **Results reproducibility:**  **yes** 
* **How to run test:**
1. {project_root}/samples에 있는 이미지들을 {project_root}/figure로 옮겨 test sample 준비
1. `cd {project_root}`
1. `pytest`
1. check output dir
