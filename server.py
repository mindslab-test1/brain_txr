import time
import grpc
from concurrent import futures
import os
import uuid

from test_pan import PanTextDetection
from test_plu import PluInpaint
from options import eval_options
from util import util

from ip_pb2 import OutputImg
from ip_pb2_grpc import ImgInpaintingServicer, add_ImgInpaintingServicer_to_server

FLAG_DEBUG = False


class ImgInpaintingServer(ImgInpaintingServicer):
    def __init__(self, opt):
        self.opt = opt
        self.textdetection = PanTextDetection(['figure/'], opt)
        self.pluInpaint = PluInpaint(opt)

    def Inpaint(self, request, context):
        print("in inpaint")

        # Read image and save according to header
        req_img = request.imgBytes
        req_format = request.format
        img_id = uuid.uuid4()
        img_name = f"{img_id}.{req_format}"
        org_img_path = os.path.join("./figure", img_name)
        with open(org_img_path, "wb") as f:
            f.write(req_img)
            if FLAG_DEBUG : print(f"saved initial input at {org_img_path}")

        # Run PAN and save masks at ./mask
        self.textdetection.set_test_loader()
        img_paths = self.textdetection.GetTextMask()
        if FLAG_DEBUG : print("PAN prepared images:", img_paths)

        # Run PluInpaint and save result
        for [inp_img_path, mask_path] in img_paths:
            # run model
            my_img, _ = self.pluInpaint.GetTextRemoval(inp_img_path, mask_path)

            # save images
            util.save_inpainted_image(inp_img_path, my_img)

            # delete mask images
            os.remove(mask_path)
            os.remove(inp_img_path)

        send_to_ip = OutputImg()
        output_name = 'result_'+img_name
        out_img_path = os.path.join("./figure/result", output_name)
        with open(out_img_path, "rb") as f:
            print(f"reading {out_img_path}")
            send_to_ip.imgBytes = f.read()

        # Clean tmp files
        util.clean_dir('./results')
        os.remove(out_img_path)

        print("finish inpaint")
        return send_to_ip


if __name__ == "__main__":

    # parse options
    opt = eval_options.EvalOptions().parse()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    ip_service_server = ImgInpaintingServer(opt)
    ip_servicer = add_ImgInpaintingServicer_to_server(ip_service_server, server)
    server.add_insecure_port("[::]:{}".format(opt.port))
    server.start()
    print("start server")

    try:
        while True:
            time.sleep(60 * 60 * 24)

    except KeyboardInterrupt:
        server.stop(0)
