import grpc
import argparse
import os

from ip_pb2 import InputImg
from ip_pb2_grpc import ImgInpaintingStub

class ImgInpaintingClient:
    def __init__(self, ip="localhost", port=56001):
        self.server_ip = ip
        self.server_port = port

        self.stub = ImgInpaintingStub(
            grpc.insecure_channel(self.server_ip + ":" + str(self.server_port))
        )

    def get_ip(self, inp_img, filename, fm):
        ip_in = InputImg()
        ip_in.imgBytes = inp_img
        ip_in.format = fm

        ip_out = self.stub.Inpaint(ip_in)
        out_img = ip_out.imgBytes

        with open("./{}".format(filename), "wb") as f:
            f.write(out_img)
        return "Done. Exported as {}.".format(filename)


if __name__ == '__main__':
    # parse options
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default='localhost', type=str, help="IP address")
    parser.add_argument("--port", default=56001, type=int, help="port number")
    parser.add_argument('--input', type=str, required=True, help='image path')
    parser.add_argument('--filename', type=str, default='result.png', help='path for result image (default: result.png)')
    args = parser.parse_args()

    ip_client = ImgInpaintingClient(ip=args.ip, port=args.port)

    with open(args.input, 'rb') as rf:
        data = rf.read()
    fm = os.path.splitext(args.input)[1][1:]

    result = ip_client.get_ip(data, args.filename, fm)
    print(result)
