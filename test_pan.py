import torch
import numpy as np
import os
import sys
from mmcv import Config
import torch.utils.data as data
from torchvision import transforms
from PIL import Image
import cv2

from util import util, io_
from options import eval_options

from pan.models import build_model
from pan.models.utils import fuse_module


class PanTextDetection:
    def __init__(self, img_dirs, args):
        # get testing options
        self.img_dirs = img_dirs
        self.args = args
        self.cfg = Config.fromfile(args.config)
        # self.gpu_ids = device if type(device) is list else [device]

        # create a model
        self.model = create_model(args)
        self.model.eval()

        # create dataloader from image dirs
        self.set_test_loader()

    def set_test_loader(self):
        # create dataloader from image dirs
        self.data_loader = PanImageLoader(self.img_dirs)
        self.test_loader = torch.utils.data.DataLoader(
            self.data_loader,
            batch_size=1,
            shuffle=False,
            num_workers=3,
            drop_last=True)

    def _GetTextPath(self):
        img_paths = []
        for idx, data in enumerate(self.test_loader):
            mask_path = os.path.join(os.path.dirname(data['img_path']), 'mask',
                                     'mask_' + os.path.basename(data['img_path'][0]))
            img_paths.append([data['img_path'], mask_path])
        return img_paths

    def GetTextMask(self):
        total_frame = 0.0
        total_time = 0.0
        img_paths = []
        for idx, data in enumerate(self.test_loader):
            print('progress: %d / %d' % (idx, len(self.test_loader)))
            sys.stdout.flush()

            print(f"PAN processing on {data['img_path'][0]} ... ")
            text_mask = self._img2mask(data)

            # save text mask file
            mask_path = util.save_mask_image(data['img_path'][0], text_mask)
            img_paths.append([data['img_path'][0], mask_path])
        return img_paths

    def _img2mask(self, data):
        # prepare input
        device = torch.device("cuda:" + str(self.args.gpu_ids[0]))
        data['imgs'] = data['imgs'].cuda(device)
        data.update(dict(
            cfg=self.cfg
        ))

        # forward
        with torch.no_grad():
            outputs = self.model(**data)

        # draw text mask
        org_size = data['img_metas']['org_img_size'][0]
        if self.cfg.test_cfg.bbox_type == 'raw':
            text_mask = cv2.resize(outputs['segmask'], dsize=(org_size[1], org_size[0]))
            text_mask = text_mask * 255
        else:
            text_mask = np.zeros([org_size[0], org_size[1], 3])
            for bbox in outputs['bboxes']:
                # For binary mask image
                cv2.drawContours(text_mask, [bbox.reshape(-1, 2)], -1, (255, 255, 255), thickness=-1)

        return text_mask


def create_model(args):
    cfg = Config.fromfile(args.config)
    model = build_model(cfg.model)
    device = torch.device("cuda:" + str(args.gpu_ids[0]))
    model = model.cuda(device)

    if args.checkpoint is not None:
        if os.path.isfile(args.checkpoint):
            print("Loading model and optimizer from checkpoint '{}'".format(args.checkpoint))
            sys.stdout.flush()

            checkpoint = torch.load(args.checkpoint, map_location=lambda storage, loc: storage.cuda(args.gpu_ids[0]))

            d = dict()
            for key, value in checkpoint['state_dict'].items():
                tmp = key[7:]
                d[tmp] = value
            model.load_state_dict(d)
        else:
            print("No checkpoint found at '{}'".format(args.checkpoint))
            raise

    # fuse conv and bn
    model = fuse_module(model)
    return model


def scale_aligned_short(img, short_size=640):
    h, w = img.shape[0:2]
    scale = short_size * 1.0 / min(h, w)
    h = int(h * scale + 0.5)
    w = int(w * scale + 0.5)
    if h % 32 != 0:
        h = h + (32 - h % 32)
    if w % 32 != 0:
        w = w + (32 - w % 32)
    img = cv2.resize(img, dsize=(w, h), interpolation=cv2.INTER_LANCZOS4)
    return img


def scale_aligned_by_area(img, short_size=2000):
    h, w = img.shape[0:2]
    scale = np.sqrt(short_size * short_size / (h * w))
    h = int(h * scale + 0.5)
    w = int(w * scale + 0.5)
    if h % 32 != 0:
        h = h + (32 - h % 32)
    if w % 32 != 0:
        w = w + (32 - w % 32)
    return cv2.resize(img, dsize=(w, h), interpolation=cv2.INTER_LANCZOS4)


def suitable_resize(img, min_size=1024, max_size=2048):
    h, w = img.shape[0:2]
    scale = 1.0
    if h * w < min_size * min_size:
        scale = np.sqrt(min_size * min_size / (h * w))
    if h * w > max_size * max_size:
        scale = np.sqrt(max_size * max_size / (h * w))

    h = int(h * scale + 0.5)
    w = int(w * scale + 0.5)
    if h % 32 != 0:
        h = h + (32 - h % 32)
    if w % 32 != 0:
        w = w + (32 - w % 32)
    return cv2.resize(img, dsize=(w, h), interpolation=cv2.INTER_LANCZOS4)


class PanImageLoader(data.Dataset):
    def __init__(self, data_dirs, read_type='pil', short_size=2000):

        self.img_paths = []

        for data_dir in data_dirs:
            img_names = io_.ls(data_dir, '.jpg')
            img_names.extend(io_.ls(data_dir, '.jpeg'))
            img_names.extend(io_.ls(data_dir, '.png'))

            img_paths = []
            for idx, img_name in enumerate(img_names):
                img_path = data_dir + img_name
                img_paths.append(img_path)

            self.img_paths.extend(img_paths)

        self.read_type = read_type
        self.short_size = short_size

    def prepare_test_data(self, index):
        img_path = self.img_paths[index]

        img_org = util.get_img(img_path, self.read_type)
        img_meta = dict(
            org_img_size=np.array(img_org.shape[:2])
        )

        # img = img_org.copy()
        # img = scale_aligned_short(img_org, self.short_size)
        # img = scale_aligned_by_area(img_org, self.short_size)
        img = suitable_resize(img_org)
        img_meta.update(dict(
            img_size=np.array(img.shape[:2])
        ))

        img = Image.fromarray(img)
        img = img.convert('RGB')
        img = transforms.ToTensor()(img)
        img = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])(img)

        data = dict(
            imgs=img,
            img_org=img_org,
            img_metas=img_meta,
            img_path=img_path
        )
        return data

    def __len__(self):
        return len(self.img_paths)

    def __getitem__(self, index):
        return self.prepare_test_data(index)


if __name__ == '__main__':
    opt = eval_options.EvalOptions().parse()
    pan = PanTextDetection(1, ['figure/'], opt)
    img_paths = pan.GetTextMask()
    print(img_paths)
