FROM nvcr.io/nvidia/cuda:10.1-cudnn7-runtime-ubuntu18.04

WORKDIR /app

COPY requirements.txt ./

RUN set -xe \
 && apt-get -y update \
 && apt-get install -y python3-pip python3-dev
RUN pip3 install --upgrade pip
RUN apt-get -y update \
 && apt-get install -y git vim libsm6 libxext6 libxrender-dev libgl1-mesa-glx

ENV PYTHONIOENCODING UTF-8

RUN apt-get install -y python3-venv \
 && python3 -m venv venv_brain-ip \
 && venv_brain-ip/bin/pip3 install --upgrade pip setuptools \
 && venv_brain-ip/bin/pip3 install -r ./requirements.txt

COPY . ./brain-ip
WORKDIR brain-ip

RUN /app/venv_brain-ip/bin/python3 -m grpc.tools.protoc -I. --python_out . --grpc_python_out . ./ip.proto \
 && cd ./pan/models/post_processing/pa \
 && /app/venv_brain-ip/bin/python3 setup.py build_ext --inplace \
 && cd ../../../../

EXPOSE 56001
ENTRYPOINT /app/venv_brain-ip/bin/python3 server.py --checkpoint ./model_logs/PAN_pth/pan_r18_ctw_finetune.pth.tar --name celeba_random --gpu_ids 0 --port 56001
