from test_pan import PanTextDetection
from test_plu import PluInpaint
from options import eval_options
from util import util

# gpu_id = 1

if __name__ == '__main__':
    # parse options and generate models
    opt = eval_options.EvalOptions().parse()
    textdetection = PanTextDetection(['figure/'], opt)
    pluInpaint = PluInpaint(opt)

    # Run PAN and save masks at ./mask
    img_paths = textdetection.GetTextMask()

    # Clean tmp results
    res_dir = './results'
    util.clean_dir(res_dir)

    # Run PluInpaint and save result at ./results
    out_paths = []
    for [inp_img_path, mask_path] in img_paths:
        # read images
        inp_img = util.path2list(inp_img_path)
        mask = util.path2list(mask_path)

        # run model
        my_img, my_img_sum = pluInpaint.GetTextRemoval(inp_img_path, mask_path)

        # save images
        out_paths.append(util.save_inpainted_image(inp_img_path, my_img))

    # Calculate SSIMs
    for [[in_path, _], out_path] in zip(img_paths, out_paths):
        ssim_value = util.calculate_ssim(util.get_img_cv2(in_path), util.get_img_cv2(out_path))
        print(f"Image {in_path} SSIM: {ssim_value}")
