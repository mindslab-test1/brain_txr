import os
from PIL import Image
import cv2
import numpy as np
import util.io_


def path2list(img_path):
    im = Image.open(img_path)
    return list(im.getdata()), im.mode, im.size, im.format


def get_img_cv2(img_path):
    try:
        img = cv2.imread(img_path)
        img = img[:, :, [2, 1, 0]]
    except Exception as e:
        print(img_path)
        raise
    return img


def get_img(img_path, read_type='pil'):
    try:
        if read_type == 'cv2':
            img = cv2.imread(img_path)
            img = img[:, :, [2, 1, 0]]
        elif read_type == 'pil':
            img = np.array(Image.open(img_path))
    except Exception as e:
        print('Cannot read image: %s.' % img_path)
        raise
    return img


def save_mask_image(org_path, mask_arr):
    os.makedirs(os.path.join(os.path.dirname(org_path), 'mask'), exist_ok=True)
    mask_path = os.path.join(os.path.dirname(org_path), 'mask', 'mask_' + os.path.basename(org_path))
    cv2.imwrite(mask_path, mask_arr)
    print(f"mask file saved at {mask_path}")
    return mask_path


def save_inpainted_image(org_path, output_img):
    im = Image.new(output_img[1], output_img[2])
    im.putdata(output_img[0])
    os.makedirs(os.path.join(os.path.dirname(org_path), 'result'), exist_ok=True)
    output_path = os.path.join(os.path.dirname(org_path), 'result', 'result_' + os.path.basename(org_path))
    im.save(output_path)
    print(f"output file saved at {output_path}")

    return output_path


def clean_dir(root_dir):
    if os.path.isdir(root_dir):
        files = os.listdir(root_dir)
        for file in files:
            util.io_.remove(os.path.join(root_dir, file))


def scale(img, long_size=2240):
    h, w = img.shape[0:2]
    scale = long_size * 1.0 / max(h, w)
    img = cv2.resize(img, dsize=None, fx=scale, fy=scale)
    return img


def ssim(img1, img2):
    c1 = (0.01 * 255)**2
    c2 = (0.03 * 255)**2

    img1 = img1.astype(np.float64)
    img2 = img2.astype(np.float64)
    kernel = cv2.getGaussianKernel(11, 1.5)
    window = np.outer(kernel, kernel.transpose())

    mu1 = cv2.filter2D(img1, -1, window)[5:-5, 5:-5]  # valid
    mu2 = cv2.filter2D(img2, -1, window)[5:-5, 5:-5]
    mu1_sq = mu1**2
    mu2_sq = mu2**2
    mu1_mu2 = mu1 * mu2
    sigma1_sq = cv2.filter2D(img1**2, -1, window)[5:-5, 5:-5] - mu1_sq
    sigma2_sq = cv2.filter2D(img2**2, -1, window)[5:-5, 5:-5] - mu2_sq
    sigma12 = cv2.filter2D(img1 * img2, -1, window)[5:-5, 5:-5] - mu1_mu2

    ssim_map = ((2 * mu1_mu2 + c1) * (2 * sigma12 + c2)) / ((mu1_sq + mu2_sq + c1) * (sigma1_sq + sigma2_sq + c2))
    return ssim_map.mean()


def calculate_ssim(img1, img2):
    '''calculate SSIM
    the same outputs as MATLAB's
    img1, img2: [0, 255]
    '''
    if not img1.shape == img2.shape:
        raise ValueError('Input images must have the same dimensions.')
    if img1.ndim == 2:
        return ssim(img1, img2)
    elif img1.ndim == 3:
        if img1.shape[2] == 3:
            ssims = []
            for i in range(3):
                ssims.append(ssim(img1, img2))
            return np.array(ssims).mean()
        elif img1.shape[2] == 1:
            return ssim(np.squeeze(img1), np.squeeze(img2))
    else:
        raise ValueError('Wrong input image dimensions.')