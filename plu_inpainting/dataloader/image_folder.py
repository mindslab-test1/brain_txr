import os
import os.path
from PIL import Image
import numpy as np

IMG_EXTENSIONS = [
    '.jpg', '.JPG', '.jpeg', '.JPEG',
    '.png', '.PNG', '.ppm', '.PPM', '.bmp', '.BMP',
]

## Including background
CLS_NUM=1


def is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)


def make_dataset(path_files):
    if path_files.find('.txt') != -1:
        paths, size = make_dataset_txt(path_files)
    elif is_image_file(path_files):
        paths, size = make_dataset_one_image_file(path_files)
    else:
        paths, size = make_dataset_dir(path_files)

    if CLS_NUM != 1:
        ## added for inflation path
        infl_paths = []

        for path in paths:
            for cls_idx in range(CLS_NUM):
                nm, ext = os.path.splitext(path)
                infl_paths.append(nm + 'cls_' + str(cls_idx) + ext)

    else:
        infl_paths=paths

    return infl_paths, len(infl_paths)


def make_dataset_txt(files):
    """
    :param path_files: the path of txt file that store the image paths
    :return: image paths and sizes
    """
    img_paths = []

    with open(files) as f:
        paths = f.readlines()

    for path in paths:
        path = path.strip()
        img_paths.append(path)

    return img_paths, len(img_paths)

def make_dataset_one_image_file(files):
    """
    :param path_files: the path of img file that store the image paths
    :return: image paths and sizes
    """
    img_paths = [files]

    return img_paths, 1

def make_dataset_dir(dir):
    """
    :param dir: directory paths that store the image
    :return: image paths and sizes
    """
    img_paths = []

    assert os.path.isdir(dir), '%s is not a valid directory' % dir

    for root, _, fnames in os.walk(dir):
        for fname in sorted(fnames):
            if is_image_file(fname):
                path = os.path.join(root, fname)
                img_paths.append(path)

    return img_paths, len(img_paths)

def inflate_image_per_class(img_txt, mask_txt, cls_num=CLS_NUM):
    '''inflate original image following num_cls'''
    with open(mask_txt) as f:
        paths = f.readlines()

        for path in paths:
            path = path.strip()
            nm, ext = os.path.splitext(path)
            print('inflating '+path)

            print(Image.open(path).mode)
            x_origin = np.array(Image.open(path))

            for cls_idx in range(cls_num):
                x = (x_origin == cls_idx).astype(float)
                x_pil = Image.fromarray(x).convert('P')
                x_pil.save(nm + 'cls_' + str(cls_idx)+ext)

    with open(img_txt) as f:
        paths = f.readlines()

        for path in paths:
            path = path.strip()
            nm, ext = os.path.splitext(path)
            print('inflating ' + path)

            x = Image.open(path)
            # +1 for background
            for cls_idx in range(cls_num):
                x.save(nm + 'cls_' + str(cls_idx) + ext)



if __name__ == '__main__':
    inflate_image_per_class('../datasets/voc/input.txt','../datasets/voc/mask.txt')